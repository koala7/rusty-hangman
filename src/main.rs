use rand::Rng;
use std::io;
use std::fs;
mod graphics;

const WORDS_FILE_PATH: &str = "words.txt";

fn main() {
    graphics::display_intro();
    run_game();
}
    
fn run_game() {
    let mut state = State {
        solution: come_up_with_random_word(),
        guessed_so_far: Vec::new(),
    };
    loop {
        render_hangman(&state);
        println!("guessed so far {:?}", state.guessed_so_far);

        check_endcondition(&state);

        let guess: char = read_guess(&state.guessed_so_far);
        state.guessed_so_far.push(guess);
    }
}

pub struct State {
    solution: String,
    guessed_so_far: Vec<char>,
}

impl State {
    fn number_wrong_guesses(self: &State) -> usize {
        let mut wrong_guesses = 0;
        for c in &self.guessed_so_far {
            if !self.solution.contains(&c.to_string()) {
                wrong_guesses = wrong_guesses + 1;
            }
        }
        wrong_guesses
    }

    fn game_won(self: &State) -> bool {
        for c in self.solution.chars() {
            if !&self.guessed_so_far.contains(&c) {
                return false;
            }
        }
        return true;
    }
}

fn come_up_with_random_word() -> String {
    let data = fs::read_to_string(WORDS_FILE_PATH).expect("Unable to read file");
    let words: Vec<&str> = data.split("\n").collect();
    let random_index = rand::thread_rng().gen_range(0, words.len());
    let random_word = words[random_index].to_uppercase();
    random_word.to_string()
}

fn check_endcondition(state: &State) {
    let max_failues = graphics::GRAPHICS.len() - 3;
    if state.number_wrong_guesses() > max_failues {
        graphics::display_game_over(&state.solution);
        std::process::exit(1);
    }

    if state.game_won() {
        graphics::display_won_screen();
        std::process::exit(0);
    }
}


fn read_guess(already_guessed: &Vec<char>) -> char {
    loop {
        println!("Please enter your guess");
        let mut guess = String::new();
        io::stdin().read_line(&mut guess).expect("Failed to read line from command line");
        guess = guess.trim().to_uppercase();
        if guess.len() != 1 {
            println!("Please enter exactly one character.");
            continue
        }
        let guess: char = guess.chars().nth(0).expect("Unexpectedly failed");
        if already_guessed.contains(&guess) {
            println!("You already tried that! Please guess a different character.");
            continue;
        }
        break guess
    }
}

fn render_hangman(state: &State) {
    graphics::render_hangman_gallow(state.number_wrong_guesses());
    graphics::render_hangman_word(&state);
}
