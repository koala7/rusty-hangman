pub fn display_intro() {
    println!("
    ██╗  ██╗ █████╗ ███╗   ██╗ ██████╗ ███╗   ███╗ █████╗ ███╗   ██╗
    ██║  ██║██╔══██╗████╗  ██║██╔════╝ ████╗ ████║██╔══██╗████╗  ██║
    ███████║███████║██╔██╗ ██║██║  ███╗██╔████╔██║███████║██╔██╗ ██║
    ██╔══██║██╔══██║██║╚██╗██║██║   ██║██║╚██╔╝██║██╔══██║██║╚██╗██║
    ██║  ██║██║  ██║██║ ╚████║╚██████╔╝██║ ╚═╝ ██║██║  ██║██║ ╚████║
    ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚═╝     ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝
                                                                    
             ██╗███╗   ██╗    ██████╗ ██╗   ██╗███████╗████████╗██╗ 
             ██║████╗  ██║    ██╔══██╗██║   ██║██╔════╝╚══██╔══╝██║ 
             ██║██╔██╗ ██║    ██████╔╝██║   ██║███████╗   ██║   ██║ 
             ██║██║╚██╗██║    ██╔══██╗██║   ██║╚════██║   ██║   ╚═╝ 
    ██╗██╗██╗██║██║ ╚████║    ██║  ██║╚██████╔╝███████║   ██║   ██╗ 
    ╚═╝╚═╝╚═╝╚═╝╚═╝  ╚═══╝    ╚═╝  ╚═╝ ╚═════╝ ╚══════╝   ╚═╝   ╚═╝ 

    - exciting (/)
    - well executed (/)
    - thrilling (/)
    - rusty (/)

One could easily think, 
this is yet another hangman clone.

But don't be fooled! 

…this one is written in rust.
--------------------------------------------------------------------
")
}

pub fn display_game_over(solution: &str) {
    println!("Oh no, he's dead. It seems you took a little too long to figure this out.");
    println!("The word was '{}'.", solution);
}

pub fn display_won_screen() {
    println!("Oh yeah, you won! Gefeliciteerd!");
    println!("{}", GRAPHICS[GRAPHICS.len() - 1]);
}

pub fn render_hangman_gallow(wrong_guesses: usize) {
    println!("{}", GRAPHICS[wrong_guesses]);
}

pub fn render_hangman_word(state: &super::State) {
    let mut letter_line = String::new();
    let mut underline_line = String::new();
    for c in state.solution.chars() {
        if state.guessed_so_far.contains(&c) {
            letter_line.push(c);
        }
        else {
            letter_line.push(' ');
        }
        underline_line.push('¯');
    }
    println!("{}", letter_line);
    println!("{}", underline_line);
}

// [no_gallow, gallow1, gallow2, …, dead_man, free_man]
pub const GRAPHICS: [&str; 11] = ["
 
 
 
 
 
 
 
_____
", "




 |
 |
 |
_|___
", "

 |
 |
 |
 |
 |
 |
_|___
", "

 |/
 |
 |
 |
 |
 |
_|___
", "
  ___
 |/
 |
 |
 |
 |
 |
_|___
", "
  _______
 |/
 |
 |
 |
 |
 |
_|___
", "
  _______
 |/      |
 |
 |
 |
 |
 |
_|___
", "
  _______
 |/      |
 |
 |
 |
 |
 |    |¯¯¯¯¯¯|
_|___ |      |
", "
  _______
 |/      |
 |      (_)
 |      \\|/
 |       |
 |      / \\
 |    |¯¯¯¯¯¯|
_|___ |      |
", "
  _______
 |/      |
 |       |
 |      (_)
 |      \\|/
 |       |
 |      / \\
_|___
", "
            ______
         __/ Yey!/ 
  (_)___/  ¯¯¯¯¯¯
  \\|/  
   |
  / \\
¯¯¯¯¯¯¯¯¯
"];
